/*******************************************************************************
File:			Global.h
By:				JM
Description:	header file for both the bootloader and main
				Application
History:
07-28-2010		Created
*******************************************************************************/

#ifdef		DEBUG_MODE
    #pragma		config			WDTEN = OFF
    #pragma		config			PWRTEN = OFF
#else
    #pragma		config			WDTEN = ON
    #pragma		config			PWRTEN = ON
#endif	
#pragma		config			CPB = OFF
#pragma		config			CP0 = OFF
#pragma		config			CP1 = OFF
#pragma		config			CP2 = OFF
#pragma		config			CP3 = OFF
#pragma		config			MCLRE = OFF
#pragma		config			CANMX = PORTB
#pragma		config			MSSPMSK = MSK5
#pragma		config			FOSC = INTIO2
#pragma		config			FCMEN = OFF
#pragma		config			IESO = OFF
#pragma		config			BOREN = SBORDIS
#pragma		config			BORV = 3            // 1.8V
#pragma 	config  		WDTPS = 256		
#pragma 	config  		STVREN = OFF
#pragma 	config  		XINST = OFF

	//Global Definitions
#ifdef	__18F23K20_H
	#define _MAX_RAM	(unsigned int)	512
#endif
#ifdef	__18F24K20_H
	#define _MAX_RAM	(unsigned int)	768
#endif
#ifdef	__18F25K20_H
	#define _MAX_RAM	(unsigned int)	1536
#endif
#ifdef	__18F26K20_H
	#define _MAX_RAM	(unsigned int)	3936
#endif

#define     INPUT       1		//port directions
#define     OUTPUT      0
/*******************************************************************************
 * Hardware Pin Re-Definitions
 * P_ indicates a physical pin
*******************************************************************************/
// PORT A

// PORT B

// Port C
#define		P_FS_SIDE               PORTCbits.RC2

#define		ENABLE_INT_HI			INTCONbits.GIEH = 1
#define		DISABLE_INT_HI			INTCONbits.GIEH = 0
#define		ENABLE_INT_LO			INTCONbits.GIEL = 1
#define		DISABLE_INT_LO			INTCONbits.GIEL = 0
#define		ENABLE_INT				INTCONbits.GIE = 1
#define		DISABLE_INT				INTCONbits.GIE = 0
#define		ENABLE_PERIPHERALS		INTCONbits.PEIE = 1
#define		DISABLE_PERIPHERALS		INTCONbits.PEIE = 0
#define     CAN_INT_FLAG            PIR5bits.RXB0IF
	
// Memory Map
#define		_ISR_H_VECTOR_ADD                   0x0008
#define		_ISR_L_VECTOR_ADD                   0x0018
#define		_BOOT_LOADER_VECTOR_ADD				0x002a
#define		_SW_RESET_VECTOR_ADD				0x0030
#define		_SW_PROGRAM_START_VECTOR_ADD		0x0036
#define		_STARTUP_ADD						0x003c
#define		_IMAGE_START						0x1000
#define		_IMAGE_CKSM_ADD						_IMAGE_START
#define		_ISR_L_ADD							0x1002
#define		_ISR_H_ADD							0x1100
#define		_USER_CODE_ADD						0x1200	
#define		_IMAGE_END							0x78ff
#define		_EEPROM_BACKUP_ADD                  0x7900      // 256 bytes
#define		_CRC_8_ADD                          0x7a00      // 256 bytes
#define		_CRC_24_L_ADD                       0x7b00      // 256 bytes
#define		_CRC_24_M_ADD                       0x7c00      // 256 bytes
#define		_CRC_24_H_ADD                       0x7d00      // 256 bytes
#define		_IMAGE_SIZE							(_IMAGE_END - _IMAGE_START)
#define		_MEMORY_END							0x7fff

// TypeDefs
typedef union {
	unsigned char Byte;
  	struct{
	    unsigned b0:1;
	    unsigned b1:1;
	    unsigned b2:1;
	    unsigned b3:1;
	    unsigned b4:1;
	    unsigned b5:1;
	    unsigned b6:1;
	    unsigned b7:1;
	};
} BitWise_8;

typedef union {
	unsigned int Word;
	unsigned char Byte[2];
  	struct{
	    unsigned b0:1;
	    unsigned b1:1;
	    unsigned b2:1;
	    unsigned b3:1;
	    unsigned b4:1;
	    unsigned b5:1;
	    unsigned b6:1;
	    unsigned b7:1;
	    unsigned b8:1;
	    unsigned b9:1;
	    unsigned ba:1;
	    unsigned bb:1;
	    unsigned bc:1;
	    unsigned bd:1;
	    unsigned be:1;
	    unsigned bf:1;
	};
} BitWise_16;

typedef union {
	unsigned long 	Long;
	unsigned int 	Word[2];
	unsigned char	Byte[4];
  	struct{
	    unsigned b0:1;
	    unsigned b1:1;
	    unsigned b2:1;
	    unsigned b3:1;
	    unsigned b4:1;
	    unsigned b5:1;
	    unsigned b6:1;
	    unsigned b7:1;
	    unsigned b8:1;
	    unsigned b9:1;
	    unsigned ba:1;
	    unsigned bb:1;
	    unsigned bc:1;
	    unsigned bd:1;
	    unsigned be:1;
	    unsigned bf:1;
	    unsigned b10:1;
	    unsigned b11:1;
	    unsigned b12:1;
	    unsigned b13:1;
	    unsigned b14:1;
	    unsigned b15:1;
	    unsigned b16:1;
	    unsigned b17:1;
	    unsigned b18:1;
	    unsigned b19:1;
	    unsigned b1a:1;
	    unsigned b1b:1;
	    unsigned b1c:1;
	    unsigned b1d:1;
	    unsigned b1e:1;
	    unsigned b1f:1;
	};
} BitWise_32;

// CAN
    // Bootloader CAN ID
#define     BOOT_CAN_ID             0x0201
#define     BOOT_CAN_DATA_ID        0x0203
#define     BOOT_ACK                0xfc00
#define     BOOT_NACK               0x050A

    // Header
#define     CAN_HEADER              0x43

    // Commands
#define     CAN_CMD_DATA            0x11
#define     CAN_CMD_EE_Read         0x17
#define     CAN_CMD_PAIR            0x18
#define     CAN_CMD_MODE            0x19
#define     CAN_CMD_RESET           0x20
#define     CAN_CMD_EE_WRITE        0x21
#define     CAN_CMD_BOOT_MODE       0x22

    // Factory Commands
#define     CAN_CMD_CAL_ENTER       0x30
#define     CAN_CMD_CAL_SET_MIN     0x31
#define     CAN_CMD_CAL_SET_MAX     0x32
#define     CAN_CMD_CAL_EXIT        0x33
#define     CAN_CMD_RAW_A2D         0x34

    // Bootloader Commands
#define     BOOT_CMD_RESET          0x60
#define     BOOT_CMD_FLASH_PAGE     0x61
#define     BOOT_CMD_RESTART        0x62
#define     BOOT_CMD_PAGE_CRC       0x63

    // Response IDs
#define     CAN_RPLY_STATUS         0x51
#define     CAN_RPLY_EEPROM         0x55
#define     CAN_ID_ASYNC            CAN_RPLY_STATUS
#define     CAN_RPLY_PAIR           CAN_RPLY_STATUS
#define     CAN_RPLY_MODE           CAN_RPLY_STATUS
#define     CAN_RPLY_RESET          CAN_RPLY_STATUS
#define     CAN_RAW_A2D             0x56

    // Asyncronous Responce IDs
//#define     CAN_ID_ASYNC            0x57
#define     CAN_ID_HEART_BEAT       0x59

    // Bootloader Responce
#define     CAN_BOOT_CMD_RESPOSE  0x5a


#define     CAN_Payload_Size        8
#define     RS_232_Packet_Size      CAN_Payload_Size + 2
typedef union {
    unsigned char   string[CAN_Payload_Size + 2];
    struct{
        BitWise_16      CAN_ID;
        unsigned char	Payload[CAN_Payload_Size];
    };
    struct{
        BitWise_16      CAN_ID;
        unsigned char   Response_ID;
        struct{
            unsigned    Calibration:1;
            unsigned    Sensor_Errors:1;
            unsigned    Tilt:1;
            unsigned    BTO:1;
            unsigned    CAN_Error:1;
            unsigned    :3;
        }Status_1;
        struct{
            unsigned    Pump_Connected:1;
            unsigned    RF_Connected:1;
            unsigned    Low_Battery:1;
            unsigned    FSTO:1;
            unsigned    Clear_Pairing:1;
            unsigned    Pairing:1;
            unsigned    Paired:1;
            unsigned    Auto_Disconnect:1;                      
        }Status_2;
        struct{
            unsigned    Left_PB:1;
            unsigned    :1;
            unsigned    Right_PB:1;
            unsigned    :3;
            unsigned    RF_Selected:1;
            unsigned    Mode:1;            
        }Switch;
        unsigned char   Left_Pedal;
        unsigned char   Center_Pedal;
        unsigned char   Right_Pedal;
        unsigned char   Threshold;
    };
    struct{
        BitWise_16      CAN_ID;
        unsigned char   Header;
        unsigned char   Command;
        unsigned char   Address;
        unsigned char   Data[4];
    };
    struct{
        BitWise_16      CAN_ID;
        unsigned char   Header;
        unsigned char   Command;
        unsigned char   Set_Mode;
        unsigned char   Set_Threshold;
    };    
    struct{
        BitWise_16      CAN_ID;
        unsigned char   Header;
        unsigned char   Command;
        BitWise_16      Flash_Address;
    };
    struct{
        BitWise_16      CAN_ID;
        unsigned char   Flash_Data[8];
//        unsigned char   Command;
//        BitWise_16      Flash_Address;
    };    
    struct{
        BitWise_16      CAN_ID;
        unsigned char   Header;        
        unsigned char   Command;
        unsigned char   Flash_CRC;
    };      
    struct{
        BitWise_16      CAN_ID;
        unsigned char   Response_ID;
        unsigned char   EE_Address;
        unsigned char   EE_Data[4];
    };
    struct{
        BitWise_16      CAN_ID;
        unsigned char   Response_ID;
        unsigned int    Raw_A2D[3];
    };    
} CAN_Packet_Structure;



