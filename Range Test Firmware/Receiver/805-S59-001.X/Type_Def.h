/* 
 * File:   Type_Def.h
 * Author: JeffM
 *
 * Created on March 11, 2019, 1:02 PM
 */

#ifndef TYPE_DEF_H
#define	TYPE_DEF_H

/*******************************************************************************
TypeDefs
*******************************************************************************/
typedef union {
	unsigned char Byte;
  	struct{
	    unsigned b0:1;
	    unsigned b1:1;
	    unsigned b2:1;
	    unsigned b3:1;
	    unsigned b4:1;
	    unsigned b5:1;
	    unsigned b6:1;
	    unsigned b7:1;
	};
} BitWise_8;

typedef union {
	unsigned int Word;
	unsigned char Byte[2];
  	struct{
	    unsigned b0:1;
	    unsigned b1:1;
	    unsigned b2:1;
	    unsigned b3:1;
	    unsigned b4:1;
	    unsigned b5:1;
	    unsigned b6:1;
	    unsigned b7:1;
	    unsigned b8:1;
	    unsigned b9:1;
	    unsigned ba:1;
	    unsigned bb:1;
	    unsigned bc:1;
	    unsigned bd:1;
	    unsigned be:1;
	    unsigned bf:1;
	};
} BitWise_16;

typedef union {
	unsigned long 	Long;
	unsigned int 	Word[2];
	unsigned char	Byte[4];
  	struct{
	    unsigned b0:1;
	    unsigned b1:1;
	    unsigned b2:1;
	    unsigned b3:1;
	    unsigned b4:1;
	    unsigned b5:1;
	    unsigned b6:1;
	    unsigned b7:1;
	    unsigned b8:1;
	    unsigned b9:1;
	    unsigned ba:1;
	    unsigned bb:1;
	    unsigned bc:1;
	    unsigned bd:1;
	    unsigned be:1;
	    unsigned bf:1;
	    unsigned b10:1;
	    unsigned b11:1;
	    unsigned b12:1;
	    unsigned b13:1;
	    unsigned b14:1;
	    unsigned b15:1;
	    unsigned b16:1;
	    unsigned b17:1;
	    unsigned b18:1;
	    unsigned b19:1;
	    unsigned b1a:1;
	    unsigned b1b:1;
	    unsigned b1c:1;
	    unsigned b1d:1;
	    unsigned b1e:1;
	    unsigned b1f:1;
	};
} BitWise_32;

#endif	/* TYPE_DEF_H */

