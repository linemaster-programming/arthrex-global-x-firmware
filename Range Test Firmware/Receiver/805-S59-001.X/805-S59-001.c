/*******************************************************************************
Program:		Flip Up RF Low Cost
LSC No.:		805-S59
By:				JM
Description:	Modification of 805-S58-002 test Firmware that allows current 
                wireless Triple to communicate with th Global X console
                this code will forward all data packets onto the CAN
                bus to perform a 10k packet range test
 
Target:			PIC18F25K80
PCBA:			805-Z45

History:
Ver:			001
10-11-2021		Created

*******************************************************************************/

#include    "../../../../Librarys/PCBAs/805-Z45/805-Z45-001.X/805_Z45_PCBA.h"
#include    "../../../../Librarys/PCBAs/805-Z45/805-Z45-001.X/805_Z45_EEprom.h"
#include    "../../../../Librarys/PCBAs/805-Z45/805-Z45-001.X/805_Z45_A2D.h"
#include    "../../../../Librarys/PCBAs/805-Z45/805-Z45-001.X/805_Z45_TMR.h"
#include    "../../../../Librarys/PCBAs/805-Z45/805-Z45-001.X/805_Z45_CAN.h"
#include    "../../../../Librarys/PCBAs/805-Z45/805-Z45-001.X/805_Z45_RF.h"
#include    "../../../../Librarys/PCBAs/805-Z45/805-Z45-001.X/805_Z45_CRC_8.h"
#include    "../../../../Librarys/PCBAs/805-Z45/805-Z45-001.X/805_Z45_CRC_24.h"
#include    "../../../Globals-003.h"
#include 	"805-S59-001.h"
#include	"CRC.h"

// checksum
#pragma romdata _CheckSum = _IMAGE_CKSM_ADD
const rom unsigned int _CheckSum = CRC;

//  Can not use
//rom const unsigned char Software_Version[] = {'8','0','5','-','S','5','9',' ',' ','V','e','r',':','0','0','1'};

// Hi ISR Vector
#pragma code InterruptVectorHigh = _ISR_H_VECTOR_ADD
void InterruptVectorHigh (void)
{
  _asm
    goto InterruptHandlerHigh 
  _endasm
}

// Lo ISR Vector
#pragma code InterruptVectorLow = _ISR_L_VECTOR_ADD
void InterruptVectorLow (void)
{
  _asm
    goto InterruptHandlerLow 
  _endasm
}

#pragma code InterruptHandlerLow = _ISR_L_ADD
#pragma interruptlow InterruptHandlerLow
void InterruptHandlerLow (void)
{
//  _asm
//    goto InterruptHandlerLow_code
//  _endasm
    InterruptHandlerLow_code();
}

#pragma code InterruptHandlerHigh = _ISR_H_ADD
#pragma interrupt InterruptHandlerHigh 
void InterruptHandlerHigh (void)
{
//  _asm
//    goto InterruptHandlerHigh_code 
//  _endasm
    InterruptHandlerHigh_code();
}

#pragma code user_code = _USER_CODE_ADD
void main (void){
    unsigned char           Send_Off_Count;

		// Wait for clock to stabilize
	while (!CLOCK_STABLE)	
		ClrWdt();
    
		// Initialize all RAM
	RCON = 0xff;
	for (FSR0 = (unsigned char) 0x0000; FSR0 < _MAX_RAM; FSR0++){
		INDF0 = 0x00;
	}
	Init_IO();
	Init_SFR();
    Init_CAN();
    Init_UART(INT_HI, 38400);

	PLL_ON;
    while(!PLL_READY);
    
	ClrWdt();
#ifdef	DEBUG_MODE
	DISABLE_WDT;
#else
	ENABLE_WDT;
#endif
	Nop();
	ENABLE_PRIORITY_INT;
        // Timer 0    
    Init_TMR_0 (TMR0_ISR_LOW, TMR0_PRE_4, TMR0_16_BIT);
	TIMER_0_ON;
	TIMER_0_INT_ON;      
        // Timer 1    
    Init_TMR_1 (TMR1_ISR_HI, TMR1_PRE_8, TMR1_INT_CLK);
	TIMER_1_ON;
	TIMER_1_INT_OFF;  
        // Timer 2
    Init_TMR_2(TMR2_ISR_LOW, TMR2_PRE_16, TMR2_POST_16);
    TIMER_2_OFF;
    TIMER_2_INT_OFF;    
        // Timer 3
    Init_TMR_3 (TMR3_ISR_LOW, TMR3_PRE_8, TMR3_INT_CLK);
    TIMER_3_OFF;
    TIMER_3_INT_OFF;    


	ENABLE_INT_HI;
	ENABLE_INT_LO;
	ClrWdt();	
    
	Nop();
        // Read Non-Volatile Data
    if (Retrieve_EEprom()){      
        // EEprom good
        if (!Check_EEprom_Backup()){
            // Flash backup not good
            Create_EEprom_Backup();
        }
        else if (Flash_CRC.Long != EE_Data.crc.Long){
            // both images are good but flash out of date
            Create_EEprom_Backup();
        }
    }
    else{
        // EEprom Corrupted
        if (Check_EEprom_Backup()){
            // Flash backup is okay
            Restore_EEprom_From_Flash();
        }
        else{
            // both copies bad, clear calibration and pairing
            EE_Data.System_Status.Byte = 0xff;
            // burn EEprom and make a backup
            Update_EEprom();
            Reset();
        }
    }

    if (Virgin){
        Virgin = 0;
		Tx1_Paired = 0;
        Tx2_Paired = 0;
        Tx1_Sensor_Fault = 0;
        Tx2_Sensor_Fault = 0;
        Tx1_Low_Battery = 0;
        Tx2_Low_Battery = 0;
		Tx1_ESN_Not_Recorded = 1;
        Tx2_ESN_Not_Recorded = 1;
        _Tx1_ESN.Long = 0x00000000;
        _Tx2_ESN.Long = 0x00000000;      
        Rcvr_FW_ID_0 = 'D';	
        Rcvr_FW_ID_1 = '4';	
        Rcvr_FW_ID_2 = '8';	     
//        Update_EEprom();        
    }

        // Read Receiver ESN from RF Digital Radio, reset and try again if fail
	_Rx_ESN.Long = Read_RF_ESN();
	if (_Rx_ESN.Long){
		Rx_ESN_Not_Recorded = 0;
	}
	else{
		Reset();
	}	

        // if any NVM changed save it and make a back up
    Update_EEprom ();
    
        // initialize some defaults
    Reset_Defaults ();

        // retrieve FW Family number to use in RF header
	RF_Tx_Packet.FW_ID_0 = Rcvr_FW_ID_0;	
	RF_Tx_Packet.FW_ID_1 = Rcvr_FW_ID_1;	
	RF_Tx_Packet.FW_ID_2 = Rcvr_FW_ID_2;
    
        // make sure the radio is in receive mode
 	Set_RF_Mode (Rx);
            
	while (1){					// infinite program loop

		ClrWdt ();
            // handle any commands 
        if (Received_Command){
            Received_Command = 0;
            Handle_Commands();
        }
        
        if (!Tx1_Paired){
            Force_IO_Off();
        }
        else if (Rcvd_Tx1_Packet){
            Rcvd_Tx1_Packet = 0;
            Tx1_FSTO_flag = 0;                      // if FSTO flag set clear it
            Set_Timer(FSTO, FAIL_SAFE_TIMEOUT);     // set fail safe timer        
            Transmit_Data_Packet (CAN_RF_PAYLOAD);
        }
            // FSTO
        else if (Check_Timer(FSTO)){   // RF Fail Safe Timeout on Station 1
            if (PB_Active | Analog_Active){
                    // Set FSTO flag
                Tx1_FSTO_flag = 1;
                Force_IO_Off();
            }
        }         
			// handle pairing
		if (Pair_Head_Start | Pairing_In_Process){
			Handle_Pairing ();              
		}           
            // if time for periodic CAN update
//        if (Check_Timer(Async_Update_Timer)){
//            Set_Timer(Async_Update_Timer, ASYNC_UPDATE_TIME);  
//
//            Handle_Push_Buttons();
//            
//            Handle_Pedels();
//
//      
//                // Transmit to the console
//            if ((Analog_Active || PB_Active) ){                                 // add in OR something plugged/unplugged
//                Transmit_Data_Packet (CAN_ID_ASYNC);
//                Send_Off_Count = NUM_OF_OFF_PACKETS;
//            }
//            else if (Send_Off_Count){
//                Send_Off_Count--;
//                Transmit_Data_Packet (CAN_ID_ASYNC);
//            }	
//            else if (Check_Timer(Heart_Beat_Timer)){
//                Transmit_Data_Packet (CAN_ID_HEART_BEAT);
//            }
//        }
	// end of the system timer loop
	}	
}	

/*******************************************************************************
Function:		Init_SFR
By:				JM
Description:	Initializes the Special Function Registers
Passed:			Not a thing
Returns:		Same
*******************************************************************************/
void Init_SFR (void){
    ANCON0 = 0x00;
    ANCON1 = 0x00;

    #if (MONITOR_Vin == TRUE)
        ENABLE_AN4;
    #endif

	//OSCCON
	OSCCONbits.IDLEN = 0;		// enter Idle mode during Sleep
	OSCCONbits.IRCF2 = 1;		// default high frequency, 000 for 31khz
	OSCCONbits.IRCF1 = 1;		// 110 8Mb, 111 16Mb
	OSCCONbits.IRCF0 = 1;

#if (USE_PLL == TRUE)
	OSCCONbits.SCS1 = 0;
	OSCCONbits.SCS0 =0;
	OSCTUNEbits.PLLEN = 1;     
#else
	OSCCONbits.SCS1 = 1;
	OSCCONbits.SCS0 =0;
	OSCTUNEbits.PLLEN = 0;     
#endif   
  
#if MONITOR_Vcc == TRUE	
	ENABLE_FVR;                 // turn on FVR
#endif

	return;
}

/*******************************************************************************
Function:		Monitor_Vcc
By:				JM
Description:	Monitors Vcc, sets global Voltage fault if off
Passed:			Not a thing
Returns:		Same
*******************************************************************************/
void Monitor_Vcc (void){
    unsigned char x;
	static unsigned int     temp_analog;
	unsigned long           temp_voltage_buffer;          
    A2D_POS_REF_VCC;	
    A2D_NEG_REF_VSS;
    temp_analog = Read_A2D(FVR);
    temp_voltage_buffer = temp_analog;
    for (x = 0; x < (8 - 1); x++){
        T_Vcc_Buffer[x+1] = T_Vcc_Buffer[x];
        temp_voltage_buffer += T_Vcc_Buffer[x];
    }	
    T_Vcc_Buffer[0] = temp_analog;
    T_Vcc = (unsigned int)(temp_voltage_buffer >> 3);	
    if (!Voltage_Buf_Full){				
        if (Volt_Buf_count++ == 8){
            Voltage_Buf_Full = 1;
        }
    }		       
    else if ((T_Vcc > T_FVR_MAX_COUNT) | (T_Vcc < T_FVR_MIN_COUNT)){
        Voltage_Fault = 1;
        PB_Active = 0;
        PB_Status.Byte = 0x00;            
    }		
    else if ((T_Vcc > T_FVR_MIN_RECOVER) & (T_Vcc < T_FVR_MAX_RECOVER)){
        Voltage_Fault = 0;
    }
}


/*******************************************************************************
Function:		InterruptHandlerLow_code
By:				JM
Description:	Handles Low level interrupts
Passed:			Not a thing
Returns:		Same
*******************************************************************************/
void InterruptHandlerLow_code (void){
	BitWise_16	Wake_Period;
    unsigned char x;

	// Timer 1 
	// 1.00mSec Period
 	if (TIMER_0_IF & TIMER_0_IE){	// Timer 0 Interrupt
		TIMER_0_IF = 0;         	// clear interrupt flag
		Wake_Period.Word = ~TICKS_PER_TIMER0;
		TMR0L = Wake_Period.Byte[0];
		TMR0H = Wake_Period.Byte[1];
        for (x = 0; x < NUM_TIMERS; x++){
            if (Timers[x])
                Timers[x]--;
        }            
        Service_Lib_Timers ();
	}
//          
//    if (TIMER_2_IF & TIMER_2_IE) { 
//        TIMER_2_IF = 0;
//    }
}
		
/*******************************************************************************
Function:		InterruptHandlerHigh_code
By:				JM
Description:	Handles Hight level interrupts
Passed:			Not a thing
Returns:		Same
*******************************************************************************/
void  InterruptHandlerHigh_code (void){
    unsigned char x;
//    unsigned char scratch;    
	near unsigned char *CAN_Rx_ptr;
    
		// CAN receive 
	if (PIR5bits.RXB0IF & PIE5bits.RXB0IE){       
		CAN_Rx_ptr = &RXB0D0;
        CAN_Rx_Packet.CAN_ID.Word = (unsigned int)RXB0SIDH << 3;
        CAN_Rx_Packet.CAN_ID.b0 =  RXB0SIDLbits.SID0;
        CAN_Rx_Packet.CAN_ID.b1 =  RXB0SIDLbits.SID1;
        CAN_Rx_Packet.CAN_ID.b2 =  RXB0SIDLbits.SID2;
		for (x = 0; x < CAN_Payload_Size; x++){
			CAN_Rx_Packet.Payload[x] = *CAN_Rx_ptr++;
		}	
        RXB0CONbits.RXFUL = 0;
		PIR5bits.RXB0IF = 0;  
        if (CAN_Rx_Packet.Header == CAN_HEADER){
            Received_Command = 1;
        }
        else if (CAN_Rx_Packet.Header == WIRELESS_CAN_HEADER){
            Received_Command = 1;            
        }
	}	

	if (UART_INT_FLAG & UART_INT_ENABLED){		// Receiver RF UART
		UART_INT_FLAG = 0;						// clear interrupt
		Receive_RF_Data ();
	}	
        // RF Packet Timeout
    if (TIMER_1_IE & TIMER_1_IF){
        TIMER_1_IF = 0;
        Reset_Packet_Reception ();
        TIMER_1_OFF;
    }
//    
//    //I2C bus conflict ISR HIGH
//    if (PIR2bits.BCLIF & PIE2bits.BCLIE) {
//        PIR2bits.BCLIF = 0;
//        PIE2bits.BCLIE = 0;
//    }
//
//    //Accelerometer Interrupt ISR HIGH
//    if (INTCONbits.RBIF & INTCONbits.RBIE) {
//        scratch = PORTB;
//        INTCONbits.RBIF = 0;
//        INTCONbits.RBIE = 0;
//    }    
	return;
}

/*******************************************************************************
Function:		Transmit_Data_Packet
By:				JM
Description:	Will transmit one Data packet out of the serial 
				Port
Passed:			Packet Type
                CAN_RPLY_STATUS  
                CAN_RPLY_EEPROM  
                CAN_RPLY_PAIR
                CAN_RPLY_MODE
                CAN_RPLY_RESET
Returns:		nothing
History:
07-09-2020		Created

*******************************************************************************/
void Transmit_Data_Packet (unsigned char packet_type){
	unsigned char x;
    far unsigned char *CAN_Tx_ptr;
    
        // CAN ID                                                               Replace with proper receiver ID
    CAN_Tx_Packet.CAN_ID.Word = 0x0303;

        // Responce ID    
    CAN_Tx_Packet.Response_ID = packet_type;    
    switch (packet_type){
        case CAN_ID_HEART_BEAT:
        case CAN_RPLY_STATUS:
        case CAN_ID_ASYNC:{
                // Status 1
            CAN_Tx_Packet.Status_1.Calibration = 0;                             // not available Un_Calibrated;
            CAN_Tx_Packet.Status_1.Sensor_Errors = RF_Rx_Packet[Tx1].Status.Sensor_Error;       
            CAN_Tx_Packet.Status_1.Tilt = 0;                                    // not available Tip_Switch_Status;
            CAN_Tx_Packet.Status_1.BTO = Beacon_Time_Out;
            CAN_Tx_Packet.Status_1.CAN_Error = COM_Error;
                // Status 2
            CAN_Tx_Packet.Status_2.Pump_Connected = 0;
            CAN_Tx_Packet.Status_2.RF_Connected = 0;
            CAN_Tx_Packet.Status_2.Low_Battery = RF_Rx_Packet[Tx1].Status.Low_Battery_Warn;
            CAN_Tx_Packet.Status_2.FSTO = Tx1_FSTO_flag | Tx2_FSTO_flag;
            CAN_Tx_Packet.Status_2.Clear_Pairing = 0;
            CAN_Tx_Packet.Status_2.Pairing = Pairing_In_Process;
            CAN_Tx_Packet.Status_2.Paired = Tx1_Paired;
            CAN_Tx_Packet.Status_2.Auto_Disconnect = 0;
//                // Switch Inputs
//            CAN_Tx_Packet.Switch.Left_PB = PB_Status.b0;
//            CAN_Tx_Packet.Switch.Right_PB = PB_Status.b2;         
//            CAN_Tx_Packet.Switch.Mode = Pedal_Mode;
//                // Analog Inputs
//            CAN_Tx_Packet.Left_Pedal = Analog_In[0].final;            
//            CAN_Tx_Packet.Center_Pedal = Analog_In[1].final;
//            CAN_Tx_Packet.Right_Pedal = Analog_In[2].final;     
//
//                // Digital Threshold
//            CAN_Tx_Packet.Threshold = Digital_Threshold.Byte[0];
            break;
        }
//        case CAN_RPLY_EEPROM:{   
//            CAN_Tx_Packet.EE_Address = CAN_Rx_Packet.Address;
//            // note on final product this will be memory mapped
//            // for now hard coding  
//            switch (CAN_Tx_Packet.EE_Address){
//                case 0x00:{                
//                    CAN_Tx_Packet.EE_Data[0] = Footswitch_SW_Version.Byte[3];
//                    CAN_Tx_Packet.EE_Data[1] = Footswitch_SW_Version.Byte[2];
//                    CAN_Tx_Packet.EE_Data[2] = Footswitch_SW_Version.Byte[1];
//                    CAN_Tx_Packet.EE_Data[3] = Footswitch_SW_Version.Byte[0];                                        
//                    break;
//                }
//                case 0x04:{
//                    CAN_Tx_Packet.EE_Data[0] = Receiver_SW_Version.Byte[3];
//                    CAN_Tx_Packet.EE_Data[1] = Receiver_SW_Version.Byte[2];
//                    CAN_Tx_Packet.EE_Data[2] = Receiver_SW_Version.Byte[1];
//                    CAN_Tx_Packet.EE_Data[3] = Receiver_SW_Version.Byte[0];                                        
//                    break;
//                }        
//                case 0x08:{
//                    CAN_Tx_Packet.EE_Data[0] = Serial_Number.Byte[3];
//                    CAN_Tx_Packet.EE_Data[1] = Serial_Number.Byte[2];
//                    CAN_Tx_Packet.EE_Data[2] = Serial_Number.Byte[1];
//                    CAN_Tx_Packet.EE_Data[3] = Serial_Number.Byte[0];                                        
//                    break;
//                }   
//                case 0x0c:{
//                    CAN_Tx_Packet.EE_Data[0] = Digital_Threshold.Byte[3];
//                    CAN_Tx_Packet.EE_Data[1] = Digital_Threshold.Byte[2];
//                    CAN_Tx_Packet.EE_Data[2] = Digital_Threshold.Byte[1];
//                    CAN_Tx_Packet.EE_Data[3] = Digital_Threshold.Byte[0];                                        
//                    break;
//                }                  
//            }
//            break;
//        }       
        case CAN_RF_PAYLOAD:{   
            CAN_Tx_Packet.Payload[0] = RF_Rx_Packet[Tx1].long_1.Byte[3];
            CAN_Tx_Packet.Payload[1] = RF_Rx_Packet[Tx1].long_1.Byte[2];
            CAN_Tx_Packet.Payload[2] = RF_Rx_Packet[Tx1].long_1.Byte[1];
            CAN_Tx_Packet.Payload[3] = RF_Rx_Packet[Tx1].long_1.Byte[0];            
            break;
        }
    }
    if (TXB0CONbits.TXREQ){
        TXB0CONbits.TXREQ = 0;		// if last message not sent abort
    }            
    CAN_Tx_ptr = &TXB0D0;
    TXB0SIDLbits.SID0 = CAN_Tx_Packet.CAN_ID.b0;
    TXB0SIDLbits.SID1 = CAN_Tx_Packet.CAN_ID.b1;
    TXB0SIDLbits.SID2 = CAN_Tx_Packet.CAN_ID.b2;
    TXB0SIDH = (unsigned char)((CAN_Tx_Packet.CAN_ID.Word >> 3) & 0xff);            
    for (x = 0; x < CAN_Payload_Size; x++){
        *CAN_Tx_ptr++ = CAN_Tx_Packet.Payload[x];
    } 
    TXB0CONbits.TXREQ = 1;    
//    Set_Timer(Async_Update_Timer, ASYNC_UPDATE_TIME);
//    Set_Timer(Heart_Beat_Timer, HEART_BEAT_TIME);    
	return;
}	


/*******************************************************************************
Function:		Set_Timer
By:				JM
Description:	Sets a value into ISR driven timers
Passed:			time in mSec and timer to set
Returns:		nothing
History:
07-09-2020		Created
*******************************************************************************/
void Set_Timer(unsigned char timer_number, unsigned long time){
	DISABLE_INT_LO;
	Timers[timer_number] = time;
	ENABLE_INT_LO;
	return;
}	


/*******************************************************************************
Function:		Check_Timer
By:				JM
Description:	checks the status of one of the ISR driven timers
Passed:			timer to check
Returns:		1 if timed out or 0 if still timing down
History:
07-09-2020		Created
*******************************************************************************/
unsigned char Check_Timer(unsigned char timer_number){
	unsigned char state;
	DISABLE_INT_LO;
//    INTCONbits.GIEL = 0;
	if (Timers[timer_number]){
		state = 0;
	}
	else{
		state = 1;
	}		
	ENABLE_INT_LO;
	return (state);
}	
	

/*******************************************************************************
Function:		Delay_ISR
By:				JM
Description:	wastes time, similar to the Delay function however
				this one uses the ISR and has a resolution of 1mSec
Passed:			Time to waste in mSec
Returns:		nothing
History:
07-09-2020		Created
*******************************************************************************/
void Delay_ISR (unsigned long mSec){
	Set_Timer (Delay_Timer, mSec);
	while (!Check_Timer(Delay_Timer)){
		ClrWdt();
	}
	return;
}		

/*******************************************************************************
Function:		Handle_Commands
By:				JM
Description:	responds to commands sent from the console
Passed:         Not a thing
Returns:		The Same
History:
07-09-2020		Created
*******************************************************************************/
void Handle_Commands (void){
    unsigned char x;
//    unsigned char addr;
//    BitWise_32 temp, t_offset, t_gain;
        
    switch (CAN_Rx_Packet.Command){
        case CAN_CMD_DATA:{
            Transmit_Data_Packet(CAN_RPLY_STATUS);
            break;
        }
//        case CAN_CMD_RAW_A2D:{
//            if (CAN_Rx_Packet.Set_Mode == 0x01){
//                Pedal_RAW_Mode = 1;
//                PB_RAW_Mode = 0;
//            }
//            else if (CAN_Rx_Packet.Set_Mode == 0x02){
//                Pedal_RAW_Mode = 0;
//                PB_RAW_Mode = 1;
//            }
//            else{
//                Pedal_RAW_Mode = 0;
//                PB_RAW_Mode = 0;                
//            }
//            break;
//        }     
        case CAN_CMD_BOOT_MODE:{
            _asm
                goto _BOOT_LOADER_VECTOR_ADD
            _endasm            
        }        
//        case CAN_CMD_MODE:{
//            Pedal_Mode = CAN_Rx_Packet.Set_Mode;
//            if (CAN_Rx_Packet.Set_Threshold > 0xf0){
//                CAN_Rx_Packet.Set_Threshold = 0xf0;
//            }
//            if (CAN_Rx_Packet.Set_Threshold < 0x0f){
//                CAN_Rx_Packet.Set_Threshold = 0x0f;
//            }            
//            Digital_Threshold.Long = (unsigned long)CAN_Rx_Packet.Set_Threshold;
//            Transmit_Data_Packet(CAN_RPLY_MODE);
//            break;
//        }    
//        case CAN_CMD_RESET:{
//            Reset_Defaults ();
//            Transmit_Data_Packet(CAN_RPLY_RESET);
//            break;
//        }    
//        case CAN_CMD_EE_Read:{
//            Transmit_Data_Packet(CAN_RPLY_EEPROM);
//            break;
//        }                
//        case CAN_CMD_EE_WRITE:{
//                                    // Not in yet will write to EE
//            break;
//        }    
//            // Calibration Commands
//        case CAN_CMD_CAL_ENTER:{
//            Un_Calibrated = 1;
//            break;
//        }
//        case CAN_CMD_CAL_SET_MIN:{
//            break;
//        }
//        case CAN_CMD_CAL_SET_MAX:{
//            break;
//        }        
//        case CAN_CMD_OFFSET_GAIN:{
//            break;
//        }
//        case CAN_CMD_CAL_EXIT:{
//            break;
//        }   
            // Wireless Commands
        case CAN_CMD_PAIR:{
            if (CAN_Rx_Packet.Payload[2] & 0x01){
                Tx1_Paired = 0;
                Tx2_Paired = 0;
                _Tx1_ESN.Long = RF_Rx_Packet[Pair].ESN = 0x0f0f;
                _Tx2_ESN.Long = RF_Rx_Packet[Pair].ESN = 0xf0f0;
                Update_EEprom(); 
            }
            else{
                if ((CAN_Rx_Packet.Payload[2] & 0x02)){
                    Clear_Cal_Command_flag = 1;
                }
                Pair_Head_Start = 1;
                Pairing_Command = 1;
            }            
            Transmit_Data_Packet(CAN_RPLY_PAIR);         
            break;
        }        
    } 
}


/*******************************************************************************
Function:		Reset_Defaults
By:				JM
Description:	Sets some user variables back to default
Passed:         Not a thing
Returns:		The Same
History:
07-09-2020		Created
*******************************************************************************/
void Reset_Defaults (void){
    Footswitch_SW_Version.Long = 0x00000001;
    Receiver_SW_Version.Long = 0x00000000;
    Serial_Number.Long = 0x1e9fd9;               // YYMMxxx
    Pedal_In_Digital_Mode = 1;
    Digital_Threshold.Long = DEFAULT_THRESHOLD;    
    return;
}


/*******************************************************************************
Function:		Handle_Pairing
By:				JM
Description:	this function will Manage the wireless pairing process
				
Passed:			Not a thing
Returns:		Same
History:
05-08-2019		Created
*******************************************************************************/
unsigned char Handle_Pairing (void){
    unsigned char state;
    unsigned char pair_type;
    
    if (Clear_Cal_Command_flag){
        pair_type = CLEAR_CAL;
    }
    else{
        pair_type = STANDARD;
    }
    state = RF_Pair_Rx (pair_type, 
                        PAIRING_WINDOW_TIME, 
                        SECOND_STATION_WINDOW_TIME);

    switch (state){
        case Pair_Idle:{
            
        }
        case Pairing_Started:{
            break;
        }
        case Pair_Tx1_Confirmed:{      
            PB_Status.Byte = 0x00;
            PB_Active = 0;              
            RF_Rx_Packet[Tx1].Analog_Data[0] = 0x00;
            RF_Rx_Packet[Tx1].Analog_Data[1] = 0x00;
            RF_Rx_Packet[Tx1].Analog_Data[2] = 0x00;
            Analog_Active = 0;
            Tx1_ESN_Not_Recorded = 1;
            Tx2_ESN_Not_Recorded = 1;
            Tx1_Paired = 0;
            Tx2_Paired = 0;  
            Force_IO_Off();
            break;
        }
        case Pair_Tx2_Confirmed:{
            Force_IO_Off();
            break;
        }
        case Pairng_Passed_Tx1:{
            Tx1_ESN_Not_Recorded = 0;
            Tx1_Paired = 1;
            Update_EEprom();   
            break;
        }
        case  Pairng_Passed_Tx2:{
            break;
        }
        case Paring_Failed_Tx1:{
            Tx1_ESN_Not_Recorded = 1;
            Tx2_ESN_Not_Recorded = 1;
            Tx1_Paired = 0;
            Tx2_Paired = 0;
            _Tx1_ESN.Long = 0x00000000;        
            _Tx2_ESN.Long = 0x00000000;
            Update_EEprom();            
            break;
        }
        case Paring_Failed_Tx2:{       
            Tx2_ESN_Not_Recorded = 1;
            Tx2_Paired = 0;              
            _Tx1_ESN.Long = 0x00000000;
            Update_EEprom();            
            break;
        }        
        case Pairing_Aborted:{               
            break;
        }
        case Pair_In_Progress:{
            break;
        }
    }
    return (state);
}

/*******************************************************************************
Function:		Handle_Pressure_Sensors
By:				JM
Description:	calculates the foot pedal(s) analog values and loads into
                the global array "Analog_In"
Passed:			Not a thing
Returns:		Same
Note:			Look up table bypassed on the first run
History:
07-10-2020		Created
*******************************************************************************/	
void Handle_Pedels (void){
	unsigned char x = 0;
  
    for (x = 0; x < NUM_OF_HALLS; x++){
        if (!Analog_Active & !PB_Active){
            if (RF_Rx_Packet[Tx1].Analog_Data[x] & Pedal_In_Analog_Mode){
                Active_Analog = x;
                Analog_Active = 1;
            }
            else if ((RF_Rx_Packet[Tx1].Analog_Data[x] > (Digital_Threshold.Byte[0] + AN_HISTORYSIS)) & Pedal_In_Digital_Mode){
                Active_Analog = x;
                Analog_Active = 1;
            }                
        }
        else if (Active_Analog == x){
            if ((RF_Rx_Packet[Tx1].Analog_Data[x] == 0x00) & Pedal_In_Analog_Mode){
                Active_Analog = NUM_OF_HALLS;
                Analog_Active = 0;

            }    
            else if ((RF_Rx_Packet[Tx1].Analog_Data[x] < (Digital_Threshold.Byte[0] - AN_HISTORYSIS)) & Pedal_In_Digital_Mode){
                Active_Analog = NUM_OF_HALLS;
                Analog_Active = 0;
                Analog_In[x].final = 0x00;
            }                   
        }
        else{
            Analog_In[x].final = 0x00;   
        }
        if (Active_Analog == x){
            if (Pedal_In_Digital_Mode){
                if (RF_Rx_Packet[Tx1].Analog_Data[x] > (Digital_Threshold.Byte[0] + AN_HISTORYSIS)){
                    Analog_In[x].final = 1;
                }
                else if (RF_Rx_Packet[Tx1].Analog_Data[x] < (Digital_Threshold.Byte[0] - AN_HISTORYSIS)){
                    Analog_In[x].final = 0;
                }
            }
            else{
                Analog_In[x].final = RF_Rx_Packet[Tx1].Analog_Data[x];
            }
        }
        else{
            Analog_In[x].final = 0x00;
        }
    }	
	return;
}

void Handle_Push_Buttons (void){
    
    if (!PB_Active & !Analog_Active){
        if (RF_Rx_Packet[Tx1].Switch_Data.b1){                          // Replace this with data from Tx on/off
            PB_Status.Left_PB_bit = 1;
            PB_Active = 1;
            Active_PB = Left_PB;
        }
//        else if (RF_Rx_Packet[Tx1].Switch_Data.b1){
//            PB_Status.Center_PB_bit = 1;
//            PB_Active = 1;
//            Active_PB = Center_PB;  
//        }                
        else if (RF_Rx_Packet[Tx1].Switch_Data.b2){
            PB_Status.Right_PB_bit = 1;
            PB_Active = 1;
            Active_PB = Right_PB;  
        }
        else{
            Active_PB = No_PB;
        }
    }
    else{
        if (Active_PB == Left_PB){
            if (!RF_Rx_Packet[Tx1].Switch_Data.b1){       // button off
                PB_Status.Left_PB_bit = 0;
                PB_Active = 0;
                Active_PB = No_PB;
            }
        }
//        else if (Active_PB == Center_PB){
//            if (!RF_Rx_Packet[Tx1].Switch_Data.b1){       // button off
//                PB_Status.Center_PB_bit = 0;
//                PB_Active = 0;
//                Active_PB = No_PB;
//            }  
//        }                
        else if (Active_PB == Right_PB){
            if (!RF_Rx_Packet[Tx1].Switch_Data.b2){       // button off
                PB_Status.Right_PB_bit = 0;
                PB_Active = 0;
                Active_PB = No_PB;
            }  
        }
    }
    return;
}

void Force_IO_Off (void){
        // clear PBs
    PB_Status.Left_PB_bit = 0;
    PB_Status.Center_PB_bit = 0;
    PB_Status.Right_PB_bit = 0;  
    PB_Active = 0;                 
    Active_PB = No_PB;
        // clear analogs
    Analog_In[0].final = 0x00;                
    RF_Rx_Packet[Tx1].Analog_Data[0] = 0x00;
    Analog_In[1].final = 0x00;
    RF_Rx_Packet[Tx1].Analog_Data[1] = 0x00;
    Analog_In[2].final = 0x00;
    RF_Rx_Packet[Tx1].Analog_Data[2] - 0x00;
    Analog_Active = 0;                
    Active_Analog = NUM_OF_HALLS;    
}