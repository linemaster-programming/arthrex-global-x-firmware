/***********************************************************/
// Date: 10/12/2021 11:16:55 AM
// Computer Generated header file containing CRC of User Code
// Note the procedure for including the CRC into the code is
// as follows: 
//    1) compile the program
//    2) Creat an Image, that also calculates the CRC
//    3) compile the program again
//    4) the hex file created is now ready to download
// CRC = C0A1
/***********************************************************/

#define   CRC 0xC0A1
