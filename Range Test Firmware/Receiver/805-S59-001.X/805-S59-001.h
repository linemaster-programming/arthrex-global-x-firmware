
    // Function Prototypes
void InterruptHandlerHigh (void);
void InterruptHandlerHigh_code (void);
void InterruptHandlerLow (void);
void InterruptHandlerLow_code (void);
void Init_SFR (void);
void Monitor_Vcc (void);
void Handle_Commands (void);
void Delay_ISR (unsigned long mSec);
unsigned char Check_Timer(unsigned char timer_number);
void Set_Timer(unsigned char timer_number, unsigned long time);
void Reset_Defaults (void);
void Transmit_Data_Packet (unsigned char packet_type);
unsigned char Handle_Pairing (void);
void Handle_Pedels (void);
void Handle_Push_Buttons (void);
void Force_IO_Off (void);

    // Global Options
#define     USE_PLL         TRUE

	// Constants
#define     NUM_OF_OFF_PACKETS          1          
#define     RF_UART_BAUD                38400
//#define		SYS_FREQ                    (float)16.0e6
#define		TIMER0_PRESCALER            (float)4
#define		INTERUPT_OVERHEAD           (float)0		// measured overhead
#define		TIMER_PERIOD				(float) 31.25e-6		// 32kHz
#define		FIXED_INTERVAL				(float) 47.5e-3
#define		TICKS_PER_FIXED_INTERVAL	(unsigned int)((float)FIXED_INTERVAL / (float) TIMER_PERIOD)
#define     DEFAULT_THRESHOLD           0x7f
#define     LOOK_UP_TABLE_SIZE          0x1ff
#define		TIMER0_RESOLUTION           (float)((float)(4/SYS_FREQ)*TIMER0_PRESCALER)
#define		TIMER0_TIMEOUT              (float) 1.00e-3
#define		TICKS_PER_TIMER0            (unsigned int) ((float)(TIMER0_TIMEOUT- INTERUPT_OVERHEAD) / (float) TIMER0_RESOLUTION)
enum {
    Left_PB,
    Center_PB,
    Right_PB,
    No_PB
};

/**********************************************************************************/
// Global Variables
/**********************************************************************************/
BitWise_32	App_Global_Flags;
    #define 	Low_Bat                 App_Global_Flags.b0
    #define		Sense_Error             App_Global_Flags.b1
    #define		Bat_Buf_Full            App_Global_Flags.b2
    #define		PB_Active               App_Global_Flags.b3
    #define		FeedBack_Status         App_Global_Flags.b4
    #define		Voltage_Buf_Full        App_Global_Flags.b5
    #define		Voltage_Fault           App_Global_Flags.b6
    #define		Received_Command        App_Global_Flags.b7
    #define     Beacon_Time_Out         App_Global_Flags.b8
    #define     Fail_Safe_Time_Out      App_Global_Flags.b9
    #define     COM_Error               App_Global_Flags.ba
    #define     Pedal_Mode              App_Global_Flags.bb
        #define     Pedal_In_Analog_Mode     !Pedal_Mode
        #define     Pedal_In_Digital_Mode    Pedal_Mode
        #define     Analog_Mode     0
        #define     Digital_Mode    1
    #define     Pedal_RAW_Mode          App_Global_Flags.bc
    #define     PB_RAW_Mode             App_Global_Flags.bd
    #define     Analog_Active           App_Global_Flags.be


Analog_Struct   PB_Analog_In[NUM_OF_HALLS];
BitWise_8       PB_Status;
    #define         Left_PB_bit     b0
    #define         Center_PB_bit   b1
    #define         Right_PB_bit    b2

Analog_Struct   Analog_In[NUM_OF_HALLS];
//    #define     Left_Pedal  Analog_In[0].final
//    #define     Center_Pedal  Analog_In[1].final
//    #define     Right_Pedal  Analog_In[2].final

unsigned char		Beacon_Value;
unsigned int        T_Vcc_Buffer [8];
unsigned int        T_Vcc;
unsigned char       Volt_Buf_count;
unsigned char       Active_Analog;
unsigned char       Active_PB;

// Vcc Monitoring
#define		T_V_Ref					(float) 3.30
#define		T_V_Ref_Tollerance		(float) 10e-3
#define		T_FVR_HYST				(float) 21e-3
#define		T_FVR_HYST_COUNT		(unsigned int)((T_FVR_HYST / T_V_Ref) * A2D_SCALE)
#define		T_FVR_NOMINAL			(float) 1.20
#define		T_FVR_TOLLERANCE		(float) (2.75/100.0)
#define		T_FVR_MAX_VOLTS			(float)(T_FVR_NOMINAL*(1.0 + T_FVR_TOLLERANCE))
#define		T_FVR_MAX_A2D_Volts		T_FVR_MAX_VOLTS
#define		T_FVR_MAX_COUNT			(unsigned int)((T_FVR_MAX_A2D_Volts / T_V_Ref + T_V_Ref_Tollerance) * A2D_SCALE)	
#define		T_FVR_MAX_RECOVER		(unsigned int)(T_FVR_MAX_COUNT - T_FVR_HYST_COUNT)
#define		T_FVR_MIN_VOLTS			(float)(T_FVR_NOMINAL*(1.0 - T_FVR_TOLLERANCE))
#define		T_FVR_MIN_A2D_Volts		T_FVR_MIN_VOLTS
#define		T_FVR_MIN_COUNT			(unsigned int)((T_FVR_MIN_A2D_Volts / T_V_Ref - T_V_Ref_Tollerance) * A2D_SCALE)	
#define		T_FVR_MIN_RECOVER		(unsigned int)(T_FVR_MIN_COUNT + T_FVR_HYST_COUNT)	

unsigned char           RS232_Rx_ptr;
unsigned char           RS232_Tx_ptr;
unsigned char           RS232_Rx_buf[RS_232_Packet_Size];

CAN_Packet_Structure	CAN_Rx_Packet;
CAN_Packet_Structure	CAN_Tx_Packet;

//EEprom on next run, hard coded for this one
BitWise_32              Footswitch_SW_Version;
BitWise_32              Receiver_SW_Version;
BitWise_32              Serial_Number;
BitWise_32              Digital_Threshold;

	// Timers
#define TIMER_0_Measured_Error  (float) 1.033
//#define TIMER_0_PERIOD          (float) (125e-6 * TIMER_0_Measured_Error)
#define	RS_232_PACKET_TIMEOUT	(unsigned int) (0xffff - (unsigned int)((float)10.0e-3 / (float)500e-9))
#define ASYNC_UPDATE_TIME       (unsigned int) ((float)10e-3 / TIMER0_TIMEOUT)
#define HEART_BEAT_TIME         (unsigned int) ((float)1.000 / TIMER0_TIMEOUT)
#define	FAIL_SAFE_TIMEOUT		(unsigned int)((float) 500e-3 / TIMER0_TIMEOUT)	// mSec
// Library Timers definitions
enum{
    GPT = 0,
    Delay_Timer,
    Heart_Beat_Timer,
    Async_Update_Timer,
    FSTO,
    NUM_TIMERS
};

unsigned int    Timers[NUM_TIMERS];

    // Pairing
//#define     Rx_Pairing_Window           13.0        // Seconds, 65.534 max
//#define	Rx_MIN_PAIR_COUNT           (unsigned int)((float)Rx_Min_Pair_Pulse/TIMER1_TIMEOUT)
//#define	Rx_MAX_PAIR_COUNT           (unsigned int)((float)Rx_Max_Pair_Pulse/TIMER1_TIMEOUT)
#define     PAIRING_WINDOW_TIME         (unsigned int)((float)10.00/TIMER0_TIMEOUT)
#define     SECOND_STATION_WINDOW_TIME  (unsigned int)((float)0.00/TIMER0_TIMEOUT)
#define     AN_HISTORYSIS               (unsigned int) 0x000f

