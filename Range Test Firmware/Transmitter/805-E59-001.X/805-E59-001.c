/*******************************************************************************
Program:		Flip Up RF Low Cost
LSC No.:		805-C48 (Obsolete)
By:				JM
Description:	Firmware for transmitter
Target:			PIC18F25K20
PCBA:			805-J43 (Obsolete)

History:
Ver:			001
11-16-2017		Created
 02-09-2018     increased pedal return to end calibration
Ver:            002
 03-07-2018     added an option to drive the LDAC pin on pairing to test 
 timing, pin goes high when pairing starts and drops once ready to send first
 pairing packet
 Ver:            003
 01-25-2019     Changed CRC to 24 bit
 Ver:            004
 04-25-2019     Converted to Library format
 Ver:            005
 02-18-2020     updated library and EEprom access to eliminate potential
                corruption, also create a backup in flash
 02-27-2020     Added extra check on reading ESN
 
Renamed Project
LSC No.:		805-T55
Target:			PIC18F25K20
PCBA:			805-K55
 
Ver:            001
12-22-2020      Changed tilt switch functionality over to use Accelerometer
                and 805-K55 PCBA, Accelerometer de-bounce done with timers.
                Moved Vin monitor over to A5, and used B4 for interrupt on tilt 
01-08-2021      reversed the polarity of the Accelerometer, and fixed the
                setting of timer 0
01-28-2021      Added a qualifier after wake so the main loop is not run 
                unless wake was caused by TMR1 (ie NOT Accelerometer)
*******************************************************************************/
//#define		USE_PLL             1
//#define		DEBUG_MODE          1				// uncomment for debug
#define         ENABLE_TEST_PAIR    1
#define         ENABLE_BOR          1
#define         MODE                Tx
#define         MONITOR_Vcc         TRUE

#include    "Type_Def.h"
#include    "../../../../Librarys/PCBAs/805-K55/805-K55-001.X/805_K55_PCBA.h"
#include    "../../../../Librarys/PCBAs/805-K55/805-K55-001.X/805_K55_EEprom.h"
#include    "../../../../Librarys/PCBAs/805-K55/805-K55-001.X/805_K55_A2D.h"
#include    "../../../../Librarys/PCBAs/805-K55/805-K55-001.X/805_K55_TMR.h"
#include    "../../../../Librarys/PCBAs/805-K55/805-K55-001.X/805_K55_RF.h"
#include    "../../../../Librarys/PCBAs/805-K55/805-K55-001.X/805_K55_IIS2DH.h"
#include    "../../../../Librarys/PCBAs/805-K55/805-K55-001.X/805_K55_I2C.h"

#include	"LSC_805_K55_Config.h"
#include 	"805-E59-001.h"

	// Set Configuration Bits
#ifdef		DEBUG_MODE
    #pragma 	config  		DEBUG = ON
    #pragma		config			WDTEN = OFF
    #pragma		config			CPB = OFF
    #pragma		config			CP0 = OFF
    #pragma		config			CP1 = OFF
    #pragma		config			CP2 = OFF
    #pragma		config			CP3 = OFF
#else
    #pragma 	config  		DEBUG = OFF
    #pragma		config			WDTEN = OFF
    #pragma		config			CPB = ON
    #pragma		config			CP0 = ON
    #pragma		config			CP1 = ON
    #pragma		config			CP2 = ON
    #pragma		config			CP3 = OFF
#endif	

#pragma		config			CPD = OFF
#pragma		config			FOSC = INTIO67
#pragma		config			FCMEN = OFF
#pragma		config			IESO = OFF
#pragma		config			PWRT = ON

#ifdef      ENABLE_BOR
    #pragma		config			BOREN = SBORDIS
#else
    #pragma		config			BOREN = OFF
#endif

#pragma		config			BORV = 18
#pragma 	config  		WDTPS = 256			// 1.024 Seconds
#pragma 	config			LPT1OSC = ON
#pragma 	config  		HFOFST = OFF
#pragma 	config  		MCLRE = OFF
#pragma 	config  		STVREN = OFF
#pragma 	config  		XINST = OFF
#pragma 	config  		LVP = OFF
#pragma		config			PBADEN = OFF

#pragma romdata RCVR_SW_VER
rom const unsigned char RCVR_SW_VER[] = {'8','0','5','-','D','4','8',' ',' ','V','e','r',':','x','x','x'};

#pragma romdata SW_VER
rom const unsigned char SW_VER[] = {'8','0','5','-','T','5','5',' ',' ','V','e','r',':','0','0','1'};

// Hi ISR Vector
#pragma code InterruptVectorHigh = 0x0008
void InterruptVectorHigh (void)
{
  _asm
    goto InterruptHandlerHigh 
  _endasm
}

// Lo ISR Vector
#pragma code InterruptVectorLow = 0x0018
void InterruptVectorLow (void)
{
  _asm
    goto InterruptHandlerLow 
  _endasm
}
//GLOBAL DATA
unsigned char data[12] = {0};
unsigned char packet_count_array[DATA_SEQ];

#pragma code main = 0x002a
void main (void){
	static unsigned char    x;
    unsigned char           Send_Off_Count;
    unsigned int count;
    //NEW
    unsigned char Test_active = FALSE;
    unsigned int packet_count = 0;
    //NEW

		// Wait for clock to stabilize
	while (!CLOCK_STABLE)	
		ClrWdt();
    
		// Initialize all RAM
	RCON = 0xff;
	for (FSR0 = (unsigned char) 0x0000; FSR0 < _MAX_RAM; FSR0++){
		INDF0 = 0x00;
	}
	Init_IO ();
	Init_SFR ();
    LEARN_DIRECTION = INPUT;
    P_LEARN = 1;
    Init_UART (RS232_BAUD);
    Init_I2C ();    
	ClrWdt();
#ifdef	DEBUG_MODE
	DISABLE_WDT;
#else
	ENABLE_WDT;
#endif
	Nop();
	ENABLE_PRIORITY_INT;
    Init_TMR_0 (TMR0_ISR_LOW, TMR0_PRE_4, TMR0_16_BIT);
    TIMER_0_OFF;    
    TIMER_0_INT_ON;
    
    Init_TMR_1 (TMR1_ISR_LOW, TMR1_PRE_1, TMR1_EXT_CLK);
    TIMER_1_ON;    
    TIMER_1_INT_ON;

    Init_TMR_2 (TMR2_ISR_HI, TMR2_PRE_16, TMR2_POST_16);
    TIMER_2_OFF;
    TIMER_2_INT_ON;
#define TIMER2_PRESCALER    (float) 16
#define TIMER2_POSTSCALER   (float) 16
    
    Init_TMR_3 (TMR3_ISR_LOW, TMR3_PRE_1, TMR3_EXT_CLK);
    TIMER_3_SYNC = 1;
    TIMER_3_INT_ON;
    TIMER_3_OFF;   

    UART_Rx_IP = HIGH;
    UART_ENABLED = 1;
	ENABLE_INT_HI;
	ENABLE_INT_LO;
	ClrWdt();	
  
    // Timer 2 will abort the I2C if the bus hangs
    TMR2 = ~(unsigned char)((float)(5e-3) / (float)((float)(4/SYS_FREQ) * TIMER2_PRESCALER * TIMER2_POSTSCALER));
    TIMER_2_ON;         // should never get an interrupt if all is well
    Acc_Fault = 0;      // default to everything okay, will be set if timeout   
    I2C_Running = 1;       
    Init_IIS2DH_INT (MAX_TILT_ANGLE, ACCELEROMETER_POSITION, 0);   // set up Acc Interrupt 
    I2C_Running = 0; 
    TIMER_2_OFF; 
    
	ClrWdt();	
 
	Nop();
	// Read Non-Volatile Data
    if (Retrieve_EEprom()){      
        // EEprom good
        if (!Check_EEprom_Backup()){
            // Flash backup not good
            Create_EEprom_Backup();
        }
        else if (Flash_CRC.Long != EE_Data.crc.Long){
            // both images are good but flash out of date
            Create_EEprom_Backup();
        }
    }
    else{
        // EEprom Corrupted
        if (Check_EEprom_Backup()){
            // Flash backup is okay
            Restore_EEprom_From_Flash();
        }
        else{
            // both copies bad, clear calibration and pairing
            EE_Data.System_Status.Byte = 0xff;
            // burn EEprom and make a backup
            Update_EEprom();
            Reset();
        }
    }
    
    // temp code per TM-372
    Write_EEprom(0x10,0x55);
    
	Battery_Read_Period = BATTERY_PERIOD;

	Tx1_ESN.Long = Read_RF_ESN();	// read RF digital ESN on each boot
	if (!Tx1_ESN.Long){						// if read fails
		Reset();							// restart code and try again
	}
	while (!Data_Ready){
		ClrWdt();
	}	
	if (Virgin){							// First Ever Run
        Rx_ESN.Long = 0x0000;
		Rx_ESN_Not_Recorded = 1;
		Virgin = 0;
	}		
	if (Rx_ESN_Not_Recorded){				// Do not have partner ESN
        Tx1_Paired = 0;
	}		
	
//	while (Un_Calibrated){					// Check if switch has been calibrated
//        ClrWdt();        
//        //Calibrate_Pedal (PRE_TRAVEL, POST_TRAVEL);
//	}

#ifndef	DEBUG_MODE
		// Check Clear Input Pin, if set erase EE_Prom
	if (P_CLR_EE == 0 & P_ISPDAT == 1){	
        ClrWdt();
		Un_Calibrated = 1;
	}	
#endif    

    //Update_EEprom();
    
        // calibration has been cleared manually must be power cycled
        // with clear calibration inputs off
//    if (Un_Calibrated){
//        // infinite loop must be restarted
//        while (1){		
//            Sleep();
//            ClrWdt();
//        }
//    }	

	TIMER_0_OFF;		// turn off when active

//DEBUG    
//	while (!Bat_Buf_Full){
//		ClrWdt();
//	}	
    
		// Fill in the receiver Firmware Code    
    RF_Tx_Packet.FW_ID_0 = Rcvr_FW_ID_0;	
	RF_Tx_Packet.FW_ID_1 = Rcvr_FW_ID_1;	
    RF_Tx_Packet.FW_ID_2 = Rcvr_FW_ID_2;	  

//DEBUG    
//	while (!Voltage_Buf_Full){
//		ClrWdt();
//	}	        
    
	while (1){					// infinite program loop
		Sleep();
		ClrWdt ();
        if (Data_Ready){        // only run main loop if TMR1 Caused wake
            Data_Ready = 0;
                // check and linearize analog inputs
            //Handle_Linear_Pedals();

                // Check the pairing push button
            Monitor_Pair_Button ();

            if (Pair_Head_Start | Pairing_In_Process){	
                RF_Pair_Tx (PAIR_PB_MIN_DURATION, PAIR_PB_MAX_DURATION);
            }	
            
            //DEBUG AB
            //else if (Tx1_Paired){
            else if(1){   
            //RF_Tx_Packet.Status.Sensor_Error = Analog_Sensor_Error;
                //for (x = 0; x < NUM_OF_HALLS; x++){
                //    RF_Tx_Packet.Analog_Data[x] = Analog[x].Serial_Out;
                //}
                    // get push button status
                RF_Tx_Packet.Switch_Data.Byte = 0x00;
                if (FS_Not_Tilted){            
                    RF_Tx_Packet.Switch_Data.b0 = !PB1;
                    RF_Tx_Packet.Switch_Data.b1 = !PB2;
                    RF_Tx_Packet.Switch_Data.b2 = !PB3;
                }            
                if (RF_Tx_Packet.Switch_Data.b1){
                    Switch_Active = 1;
                }    
                else {
                    Switch_Active = 0;
                }           
                
                if(Switch_Active == 1)
                {
                    if(!Test_active)
                    {
                        Test_active = TRUE;
                        packet_count = 1;
                        RF_Tx_Packet.word_2.Word = 0;   
                        RF_Tx_Packet.Status.Byte = 0;
                        RF_Tx_Packet.Analog_Data[0] = 0;
                        RF_Tx_Packet.Analog_Data[1] = 0;
                        RF_Tx_Packet.Analog_Data[2] = 0;
                    }
                }
                
                if(Test_active == TRUE)
                {
                    // To ABORT
                    if(RF_Tx_Packet.Switch_Data.b2)
                    {
                        Test_active = FALSE;
                        packet_count = 1;
                    }
                    
                    if (packet_count < (TEST_PACKETS+1))
                    {
                        RF_Tx_Packet.word_2.Word = packet_count;
                        Transmit_RF_Data_Packet(ENCRYPTED_PACKET);
                        packet_count++;  
                    }
                    else
                    {
                        Test_active = FALSE;
                        packet_count = 1;
                    }
                }   
                
                //NEW
                    // only transmit data if not tilted and not in low battery error
//                if (!RF_Tx_Packet.Status.Low_Battery_Error & FS_Not_Tilted){
//                    if (Analog_Active || Switch_Active){
//                        Transmit_RF_Data_Packet (ENCRYPTED_PACKET);
//                        Send_Off_Count = NUM_OF_OFF_PACKETS;
//                    }
//                    else if (Send_Off_Count){
//                        Send_Off_Count--;
//                        Transmit_RF_Data_Packet (ENCRYPTED_PACKET);
//                    }	
//                }
//                    // always send off packets
//                else if (Send_Off_Count){
//                    Send_Off_Count--;
//                    Transmit_RF_Data_Packet (ENCRYPTED_PACKET);
//                }	
                //NEW
            }	
        }	
    }
}	

////NEW
//void Transmit_RF_Test_Packet (void){
//	unsigned char   x;
//    
//	LEARN_DIRECTION = INPUT;
//        
//    // load Transmitter packet into buffer
//    for (x = 0; x < 9; x++){
//        //CRC_buf[x] = RF_Tx_Packet.string[x];
//        
//    }    
//    UART_TX_BUFFER = 0xF0;
//    
//	while (UART_SHIFT_REG_FULL);
//	for (x = 0; x < RF_Packet_Size; x++){		// 26uSec/byte
//		UART_TX_BUFFER = data[x];
//		Nop();
//		while (UART_SHIFT_REG_FULL);
//	}
//	return;
//}	
//
////NEW
//void int_to_ascii(int num, unsigned char digit)
//{
//    int packet_number;
//    unsigned char rem = 0;
//    
//    packet_number = num;   
//    while (num > 0) {
//        rem = (unsigned char) (num % 10);  
//        packet_count_array[digit] = (unsigned char)('0' + rem);
//        num = num / 10;
//        digit--;
//    }
//    if(packet_number < TEST_PACKETS)
//    {
//        packet_count_array[0] = '0';
//    }
//    else if(packet_number == (TEST_PACKETS))
//    {
//        packet_count_array[0] = '1';
//    }
//}

/*******************************************************************************
Function:		Init_SFR
By:				JM
Description:	Initializes the Special Function Registers
Passed:			Not a thing
Returns:		Same
*******************************************************************************/
void Init_SFR (void){
	SSPMSK = 0x00;
	// ANSEL (analog Select default to all off)
	ANSEL = 0x00;
	ANSELH = 0x00;
    
    #if	(AN1_Enable == TRUE)
		ENABLE_AN0;
		Analog[0].Flags.Enable = 1;
    #endif  

	#if	(AN2_Enable == TRUE)
		ENABLE_AN1;
		Analog[1].Flags.Enable = 1;
    #endif 

    #if	(AN3_Enable == TRUE)
		ENABLE_AN2;
		Analog[2].Flags.Enable = 1;
    #endif    
        
    #if (MONITOR_Vin == TRUE)
        ENABLE_AN4;
    #endif

	// ADCON2
	ADCON2bits.ADCS0 = 0;
	ADCON2bits.ADCS1 = 1;
	ADCON2bits.ADCS2 = 0;
	ADCON2bits.ACQT0 = 1;
	ADCON2bits.ACQT1 = 0;
	ADCON2bits.ACQT2 = 1;
	ADCON2bits.ADFM = 1;

	//OSCCON
	OSCCONbits.IDLEN = 0;		// enter Idle mode during Sleep
	OSCCONbits.IRCF2 = 1;		// default high frequency, 000 for 31khz
	OSCCONbits.IRCF1 = 1;		// 110 8Mb, 111 16Mb
	OSCCONbits.IRCF0 = 1;

#if (USE_PLL == TRUE)
	OSCCONbits.SCS1 = 0;
	OSCCONbits.SCS0 =0;
	OSCTUNEbits.PLLEN = 1;     
#else
	OSCCONbits.SCS1 = 1;
	OSCCONbits.SCS0 =0;
	OSCTUNEbits.PLLEN = 0;     
#endif   
  
	ENABLE_FVR;                 // turn on FVR

	return;
}

/*******************************************************************************
Function:		Monitor_Vcc
By:				JM
Description:	Monitors Vcc, sets global Voltage fault if off
Passed:			Not a thing
Returns:		Same
*******************************************************************************/
void Monitor_Vcc (void){
    unsigned char x;
	static unsigned int     temp_analog;
	unsigned long           temp_voltage_buffer;     
    A2D_POS_REF_VCC;	
    A2D_NEG_REF_VSS;
    temp_analog = Read_A2D(FVR);
    temp_voltage_buffer = temp_analog;
    for (x = 0; x < (8 - 1); x++){
        T_Vcc_Buffer[x+1] = T_Vcc_Buffer[x];
        temp_voltage_buffer += T_Vcc_Buffer[x];
    }	
    T_Vcc_Buffer[0] = temp_analog;
    T_Vcc = (unsigned int)(temp_voltage_buffer >> 3);	
    if (!Voltage_Buf_Full){				
        if (Volt_Buf_count++ == 8){
            Voltage_Buf_Full = 1;
        }
    }		       
    else if ((T_Vcc > T_FVR_MAX_COUNT) | (T_Vcc < T_FVR_MIN_COUNT)){
        Voltage_Fault = 1;
        Analog_Active = 0;
        Switch_Active = 0;
        RF_Tx_Packet.Switch_Data.Byte = 0x00;
        for (x = 0; x < NUM_OF_HALLS; x++){
            RF_Tx_Packet.Analog_Data[x] = 0x00;
            Analog[x].Serial_Out = 0x00;
            Analog[x].Raw = 0x00;            
        }	            
    }		
    else if ((T_Vcc > T_FVR_MIN_RECOVER) & (T_Vcc < T_FVR_MAX_RECOVER)){
        Voltage_Fault = 0;
    }
}

/*******************************************************************************
Function:		Read_Analog_Values
By:				JM
Description:	using the A2D the hall voltages are read at regular intervals
Passed:			Not a thing
Returns:		Same
*******************************************************************************/
void Read_Analog_Values (void){
    unsigned char x;
    unsigned long temp_battery_buffer;
    if (FS_Not_Tilted | Un_Calibrated){
            // read hall voltages
        P_HALL_EN = 0;
        Delay (100);					// give time for hall to wake
        for (x = 0; x < NUM_OF_HALLS; x++){
            if (Analog[x].Flags.Enable){
                #if	(HALL_LIFT_OFF == TRUE)
                        Analog[x].Raw =( 0x3ff - Read_A2D(Analog_List[x]));
                #else
                        Analog[x].Raw = Read_A2D(Analog_List[x]);
                #endif
            }
            else{
                Analog[x].Raw = 0x00;
            }	
        }
        P_HALL_EN = 1;	            
    }   
    else{
        for (x = 0; x < NUM_OF_HALLS; x++){
            Analog[x].Raw = 0x00;
        }
    }
        // Check Battery Voltage
    if (Battery_Read_Period){
        Battery_Read_Period--;
    }
    else{
        Battery_Read_Period = BATTERY_PERIOD;
        P_VIN_EN = 1;
        temp_battery_buffer = 0;
        for (x = 0; x < 16; x++){		// take multiple samples to filter out dips in Vbat
            Delay (10);					// give time between samples
            temp_battery_buffer +=  Read_A2D(Vin_Mon);
        }	
        Vin.Raw =  (unsigned int)temp_battery_buffer >> 4;		// divide by 16
        if (Vin.Raw < Low_Bat_Warn_A2D){
            RF_Tx_Packet.Status.Low_Battery_Warn = 1;
        }	
        if (Vin.Raw < Low_Bat_Error_A2D){
            RF_Tx_Packet.Status.Low_Battery_Error = 1;
            Analog_Active = 0;
            Switch_Active = 0;
            RF_Tx_Packet.Switch_Data.Byte = 0x00;
            for (x = 0; x < NUM_OF_HALLS; x++){
                RF_Tx_Packet.Analog_Data[x] = 0x00;
                Analog[x].Serial_Out = 0x00;
            }	
        }	
        Bat_Buf_Full = 1;        
        P_VIN_EN = 0;
    }    
}
/*******************************************************************************
Function:		InterruptHandlerLow
By:				JM
Description:	Handles Low level interrupts
Passed:			Not a thing
Returns:		Same
*******************************************************************************/
#pragma interruptlow InterruptHandlerLow
void InterruptHandlerLow (void){
	static  BitWise_16	Wake_Period;
    unsigned char scratch;    

	// Timer 0 only used during marriage and ESN reading
	// 1.00mSec Period
 	if (TIMER_0_IF & TIMER_0_IE){	// Timer 0 Interrupt
		TIMER_0_IF = 0;         	// clear interrupt flag
		Wake_Period.Word = ~TICKS_PER_TIMER0;
        TMR0H = Wake_Period.Byte[1];  
        TMR0L = Wake_Period.Byte[0];
        scratch = TMR0L;
			// Service Timers
        Service_Lib_Timers ();
	}
	
	// Timer 1 only used at the foot pedal and is triggered every
	// 47.5mSec to wake processor from sleep
 	if (TIMER_1_IF & TIMER_1_IE){				// check for TMR1 overflow
		TIMER_1_IF = 0;          	// clear interrupt flag
        TIMER_1_OFF;
		Wake_Period.Word = ~TICKS_PER_FIXED_INTERVAL;
		TMR1H = Wake_Period.Byte[1];
		TMR1L = Wake_Period.Byte[0];
        TIMER_1_ON;
        Data_Ready = 1; 
        
        //DEBUG
        Check_For_Packet_Timeout ();
        //Read_Analog_Values ();
        //Monitor_Vcc ();        
    }
    
    // Timer 3 used as an accelerometer de-bounce
 	if (TIMER_3_IF & TIMER_3_IE){	// Timer 3 Interrupt
		TIMER_3_IF = 0;             // clear interrupt flag 
        TIMER_3_IE = 0;
        TIMER_3_OFF;
        if (!P_TILT){
            Tip_Switch_Status = Tilted;    
        }  
        else{
            Tip_Switch_Status = Not_Tilted;    
        }                        
    }  
    
        // Change on accelerometer interrupt line
    if (INTCONbits.RBIF & INTCONbits.RBIE){
        scratch = PORTB;
        INTCONbits.RBIF = 0;
        TIMER_3_OFF;
		Wake_Period.Word = ~TICKS_PER_TILT_DEBOUNCE;
		TMR3H = Wake_Period.Byte[1];
		TMR3L = Wake_Period.Byte[0];        
        TIMER_3_IF = 0;
        TIMER_3_IE = 1;
        TIMER_3_ON;
    }
    
    // Check if we have a comm error
    // must be in ISR that routinely is called in order to clear an error
    Clear_Packet_Overrun ();
		
 	return;
}
		
/*******************************************************************************
Function:		InterruptHandlerHigh
By:				JM
Description:	Handles Hight level interrupts
Passed:			Not a thing
Returns:		Same
*******************************************************************************/
#pragma interrupt InterruptHandlerHigh 
void  InterruptHandlerHigh (void){
	
	if (UART_INT_FLAG & UART_INT_ENABLED){         // Transmitter RF UART
		UART_INT_FLAG = 0;						// clear interrupt
        Receive_RF_Data ();
	}	
    
        // I2C collide
    if (I2C_BCLIF & I2C_BCLIE){
        I2C_BCLIF = 0;
        I2C1_Bus_Col();
    }
    
    if (I2C_SSPIF & I2C_SSPIE){
        I2C_SSPIF = 0;
    }
    
    // Timer 2 used as an I2C supervisor
 	if (TIMER_2_IF & TIMER_2_IE){	// Timer 0 Interrupt
		TIMER_2_IF = 0;             // clear interrupt flag 
        if (I2C_Running){           // shouldn't be running
            SSPCON1bits.SSPEN = 0;
            Acc_Fault = 1;          
        }          
    }
    
	return;
}

