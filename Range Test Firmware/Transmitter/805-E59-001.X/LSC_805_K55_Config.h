/*******************************************************************************
By:				JM
Description:	Sets the operational options for the 805-J43 PCBA
History:
04-30-2019		Created
*******************************************************************************/

#ifndef LSC_805_J43_CONFIG_H
#define	LSC_805_J43_CONFIG_H

enum Switch_Types{
    WIRED = 0,
    RF_TX,
    RF_RCVR,
    NUM_OF_SWITCH_TYPES
};  

/*******************************************************************************
 System
*******************************************************************************/
//  PLL
#define     SWITCH_TYPE                 RF_TX       // RF_TX, RF_RCV, WIRED
#define     USE_PLL                     FALSE       // TRUE 64 Mhz
                                                    // FALSE 16 Mhz
#define     BEACON_ENABLED              TRUE        // TRUE / FALSE
#define     BEACON_TIMEOUT_SEC          10.0        // Time in Seconds 

#define     MAX_TILT_ANGLE              45          // degrees
#define     ACCELEROMETER_POSITION      X_NEG

#define     TILT_SWITCH_DEBOUNCE        4           // 1 - 8, 47.5mSec samples

/*******************************************************************************
 Analog Inputs
 Note: analogs that are not enabled will be configured as switch inputs
*******************************************************************************/
#define		HALL_LIFT_OFF			FALSE		// hall voltage reduces as treadle is depressed
#define		AN1_Enable				TRUE		// True or False
#define		AN2_Enable				TRUE		// disabled Analogs set to switch inputs
#define		AN3_Enable				TRUE
#define		PRE_TRAVEL				11          // %
#define		POST_TRAVEL				10          // %


/*******************************************************************************
 OC Outputs can be set to one of the following Options:
    The input type must be defined as ANALOG or DIGITAL
    If An Analog is used to drive a OC the TURN_ON_POINT and TURN_OFF_POINT
    Will Set the level of activation
        ANALOG_1
        ANALOG_2
        ANALOG_3
		SWITCH_1
		SWITCH_2
		SWITCH_3	
		LOW             i.e. always OFF
		HIGH            i.e. always ON
*******************************************************************************/ 
#define     OC_1_Input_Type     DIGITAL
#define     OC_1                LOW
#define     OC_2_Input_Type     DIGITAL
#define     OC_2                LOW
#define     OC_3_Input_Type     DIGITAL
#define     OC_3                LOW
#define     TURN_ON_POINT       60          // percent of Analog travel
#define     TURN_OFF_POINT      40          // percent of Analog travel

/*******************************************************************************
 Analog Outputs
*******************************************************************************/ 
#define     ANALOG_MIN_VOLTS        0.00        // Volts
#define     ANALOG_MAX_VOLTS        4.50        // Volts


/*******************************************************************************
    RS232
    RS232 Baud rate can be set the any one of the following values
        1200
        2400
        4800
        9600
        19200
        38400
        57600
        115200
        230400
        460800
        921600
    If Serial Communications are used, Pairing is not turned on by default
    If a Pairing command is desired PAIR_SERIAL_COMMAND_ENABLED must be 
    Enabled
    Serial Pairing command and the push button can both be enabled
*******************************************************************************/ 
#define     RS232_ENABLED   TRUE            // RS232 Enabled
                                            // RS232 turned off
//#if SWITCH_TYPE == WIRED
//    #if RS232_ENABLED == TRUE
//        #define     RS232_BAUD      115200
//    #endif
//#else   // RF
    #define     RS232_BAUD      38400
//#endif

/*******************************************************************************
 Pairing Options
    Select who gets started first, AKA go into listen mode and wait
    by setting "PAIR_SEQUENCE" to one of the following options:
        RCVR_FIRST     // receiver pairing started first
        Tx_FIRST       // transmitter pairing started first
        SIMULTAINOUS   // receiver and transmitter started same time 
*******************************************************************************/ 
#define     PAIR_SEQUENCE       SIMULTAINOUS
#define     PAIR_WINDOW         30          // time in seconds to wait for Tx
#define     PAIR_PB_ENABLED     TRUE        // Push button used to start pairing
#define     NUM_OF_OFF_PACKETS  10          // number of packets to send once switch goes idle

#if PAIR_PB_ENABLED == TRUE
    #define     PAIR_PB_MIN_DURATION    3.00   // time in seconds
    #define     PAIR_PB_MAX_DURATION    6.00   // time in seconds
#endif
//
//#if DUAL_STATION == TRUE
//    #define     PAIR_SECOND_STATION     60
//#else
//    #define     PAIR_SECOND_STATION     0
//#endif

/*******************************************************************************
    MONITORING Vcc
	Setting the monitor to True will enable the voltage fault monitoring		
    Note: Voltages are actual Vcc values assuming an ideal FVR
    The "Vcc_Monitor_Tolerance" is used only in the verification of the 
    feature and must account for the total tolerance stack including FVR
*******************************************************************************/
#define     MONITOR_Vcc             TRUE        // TRUE OR FALSE

/*******************************************************************************
    MONITORING Vin
	Setting the monitor to True will enable the voltage powering the PCBA
    After the divider Circuit
    The "Vin_Monitor_Tolerance" is used only in the verification of the 
    feature and must account for the total tolerance stack
*******************************************************************************/
#define     MONITOR_Vin             TRUE        // TRUE OR FALSE
#if     MONITOR_Vin == TRUE
    #define		Low_Bat_Warn_Voltage	2.0
    #define		Low_Bat_Error_Voltage	1.8
#endif

#endif