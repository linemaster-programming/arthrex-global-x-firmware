/*******************************************************************************
By:				JM
Description:	header file for 805-C48-004.c 
History:
04-29-2019		Created
*******************************************************************************/
	// Function Prototypes
void InterruptHandlerLow (void);
void InterruptHandlerHigh (void);
void Init_SFR (void);
void Monitor_Vcc (void);
void Read_Analog_Values (void);
void Transmit_RF_Test_Packet(void);
void int_to_ascii(int num, unsigned char digit);

	// Constants
#define		TIMER_PERIOD				(float) 31.25e-6		// 32kHz
#define		FIXED_INTERVAL				(float) 47.5e-3
#define		TICKS_PER_FIXED_INTERVAL	(unsigned int)((float)FIXED_INTERVAL / (float) TIMER_PERIOD)
#define     TILT_DEBOUNCE               (float) 200e-3
#define     TICKS_PER_TILT_DEBOUNCE     (unsigned int)((float)TILT_DEBOUNCE / (float) TIMER_PERIOD)
#define     I2C_SUPERVISOR              (float) 100e-3
#define     I2C_SUPERVISOR_TIMEOUT      (unsigned int)((float)I2C_SUPERVISOR / (float) TIMER_PERIOD)
//#define     DATAEE_WriteByte            Write_EEprom
/**********************************************************************************/
// Global Variables
/**********************************************************************************/
BitWise_16	Global_Flags;
    #define 	Low_Bat				Global_Flags.b0
    #define		Sense_Error			Global_Flags.b1
    #define		Bat_Buf_Full		Global_Flags.b2
    #define		Switch_Active		Global_Flags.b3
    #define		FeedBack_Status 	Global_Flags.b4
    #define		Voltage_Buf_Full	Global_Flags.b5
    #define		Voltage_Fault		Global_Flags.b6
BitWise_8           Switch;
unsigned char		Beacon_Value;
unsigned int        T_Vcc_Buffer [8];
unsigned int        T_Vcc;
unsigned char       Volt_Buf_count;

// Vcc Monitoring
#define		T_V_Ref					(float) 2.00
#define		T_V_Ref_Tollerance		(float) 10e-3
#define		T_FVR_HYST				(float) 21e-3
#define		T_FVR_HYST_COUNT		(unsigned int)((T_FVR_HYST / T_V_Ref) * A2D_SCALE)
#define		T_FVR_NOMINAL			(float) 1.20
#define		T_FVR_TOLLERANCE		(float) (2.75/100.0)
#define		T_FVR_MAX_VOLTS			(float)(T_FVR_NOMINAL*(1.0 + T_FVR_TOLLERANCE))
#define		T_FVR_MAX_A2D_Volts		T_FVR_MAX_VOLTS
#define		T_FVR_MAX_COUNT			(unsigned int)((T_FVR_MAX_A2D_Volts / T_V_Ref + T_V_Ref_Tollerance) * A2D_SCALE)	
#define		T_FVR_MAX_RECOVER		(unsigned int)(T_FVR_MAX_COUNT - T_FVR_HYST_COUNT)
#define		T_FVR_MIN_VOLTS			(float)(T_FVR_NOMINAL*(1.0 - T_FVR_TOLLERANCE))
#define		T_FVR_MIN_A2D_Volts		T_FVR_MIN_VOLTS
#define		T_FVR_MIN_COUNT			(unsigned int)((T_FVR_MIN_A2D_Volts / T_V_Ref - T_V_Ref_Tollerance) * A2D_SCALE)	
#define		T_FVR_MIN_RECOVER		(unsigned int)(T_FVR_MIN_COUNT + T_FVR_HYST_COUNT)	

// Battery Monitor
unsigned int	Battery_Read_Period;
#define			BATTERY_PERIOD          (unsigned int)((float)2.00 / FIXED_INTERVAL)
#define         Low_Bat_Warn_Voltage	2.0
#define         Low_Bat_Warn_A2D		(unsigned int) (Low_Bat_Warn_Voltage/Vbat_V_LSB)
#define         Low_Bat_Error_Voltage	1.8
#define         Low_Bat_Error_A2D		(unsigned int) (Low_Bat_Error_Voltage/Vbat_V_LSB)

//NEW
#define     TEST_PACKETS            10000
#define     DATA_SEQ                5
#define     TRUE                    1
#define     FALSE                   0