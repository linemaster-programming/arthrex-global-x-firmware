/*******************************************************************************
File:			Global-002.h
By:				JM
Description:	header file for both the bootloader and main
				Application
History:
07-28-2010		Created for original X-Number using a PIC18F25K20
11-25-2020      Modified to use actual PCBA and PIC18F45K80
07-8-2021       Ver:003
                Reverted to the header file from X-Number Archive
*******************************************************************************/

#ifdef		DEBUG_MODE
    #pragma		config			WDTEN = OFF
    #pragma		config			PWRTEN = OFF
#else
    #pragma		config			WDTEN = ON
    #pragma		config			PWRTEN = ON
#endif	
#pragma		config			CPB = OFF
#pragma		config			CP0 = OFF
#pragma		config			CP1 = OFF
#pragma		config			CP2 = OFF
#pragma		config			CP3 = OFF
#pragma		config			MCLRE = OFF
#pragma		config			CANMX = PORTB
#pragma		config			MSSPMSK = MSK5
#pragma		config			FOSC = INTIO2
#pragma		config			FCMEN = OFF
#pragma		config			IESO = OFF
#pragma		config			BOREN = SBORDIS
#pragma		config			BORV = 3            // 1.8V
#pragma 	config  		WDTPS = 256		
#pragma 	config  		STVREN = OFF
#pragma 	config  		XINST = OFF

// CAN
    // Device CAN_IDs
#define     RIGHT_PORT_CAN_ID       0x0301
#define     LEFT_PORT_CAN_ID        0x0302
#define     RCVR_PORT_CAN_ID        0x0303
    // Bootloader CAN ID
#define     BOOT_CAN_ID             0x0201
#define     BOOT_CAN_DATA_ID        0x0203
#define     BOOT_ACK                0xfc00
#define     BOOT_NACK               0x050A

    // Header
#define     CAN_HEADER              0x43    // All Devices
#define     LEFT_CAN_HEADER         0x53
#define     RIGHT_CAN_HEADER        0x63
#define     WIRELESS_CAN_HEADER     0x73

    // Commands
#define     CAN_CMD_DATA            0x11
#define     CAN_CMD_EE_Read         0x17
#define     CAN_CMD_PAIR            0x18
#define     CAN_CMD_MODE            0x19
#define     CAN_CMD_RESET           0x20
#define     CAN_CMD_EE_WRITE        0x21
#define     CAN_CMD_BOOT_MODE       0x22

    // Factory Commands
#define     CAN_CMD_CAL_ENTER       0x30
#define     CAN_CMD_CAL_SET_MIN     0x31
#define     CAN_CMD_CAL_SET_MAX     0x32
#define     CAN_CMD_CAL_EXIT        0x33
#define     CAN_CMD_RAW_A2D         0x34
#define     CAN_CMD_OFFSET_GAIN     0x35

    // Bootloader Commands
#define     BOOT_CMD_RESET          0x60
#define     BOOT_CMD_FLASH_PAGE     0x61
#define     BOOT_CMD_RESTART        0x62
#define     BOOT_CMD_PAGE_CRC       0x63

    // Response IDs
#define     CAN_RPLY_STATUS         0x51
#define     CAN_RPLY_EEPROM         0x55
//#define     CAN_ID_ASYNC            CAN_RPLY_STATUS
#define     CAN_RPLY_PAIR           CAN_RPLY_STATUS
#define     CAN_RPLY_MODE           CAN_RPLY_STATUS
#define     CAN_RPLY_RESET          CAN_RPLY_STATUS
//#define     CAN_RAW_A2D             0x56

    // Async Response IDs
#define     CAN_ID_ASYNC            0x57
#define     CAN_ID_HEART_BEAT       0x59
#define     CAN_RF_PAYLOAD          0x70

    // Bootloader Response
#define     CAN_BOOT_CMD_RESPOSE    0x5a

//#define     CAN_Payload_Size        8
#define     RS_232_Packet_Size      CAN_Payload_Size + 2
typedef union {
    unsigned char   string[CAN_Payload_Size + 2];
    struct{
        BitWise_16      CAN_ID;
        unsigned char	Payload[CAN_Payload_Size];
    };
    struct{
        BitWise_16      CAN_ID;
        unsigned char   Response_ID;
        struct{
            unsigned    Calibration:1;
            unsigned    Sensor_Errors:1;
            unsigned    Tilt:1;
            unsigned    BTO:1;
            unsigned    CAN_Error:1;
            unsigned    :3;
        }Status_1;
        struct{
            unsigned    Pump_Connected:1;
            unsigned    RF_Connected:1;
            unsigned    Low_Battery:1;
            unsigned    FSTO:1;
            unsigned    Clear_Pairing:1;
            unsigned    Pairing:1;
            unsigned    Paired:1;
            unsigned    Auto_Disconnect:1;                      
        }Status_2;
        struct{
            unsigned    Left_PB:1;
            unsigned    :1;
            unsigned    Right_PB:1;
            unsigned    :3;
            unsigned    RF_Selected:1;
            unsigned    Mode:1;            
        }Switch;
        unsigned char   Left_Pedal;
        unsigned char   Center_Pedal;
        unsigned char   Right_Pedal;
        unsigned char   Threshold;
    };
    struct{
        BitWise_16      CAN_ID;
        unsigned char   Header;
        unsigned char   Command;
        unsigned char   Address;
        unsigned char   Data[4];
    };
    struct{
        BitWise_16      CAN_ID;
        unsigned char   Header;
        unsigned char   Command;
        unsigned char   Set_Mode;
        unsigned char   Set_Threshold;
    };    
    struct{
        BitWise_16      CAN_ID;
        unsigned char   Header;
        unsigned char   Command;
        BitWise_16      Flash_Address;
    };
    struct{
        BitWise_16      CAN_ID;
        unsigned char   Flash_Data[8];
//        unsigned char   Command;
//        BitWise_16      Flash_Address;
    };    
    struct{
        BitWise_16      CAN_ID;
        unsigned char   Header;        
        unsigned char   Command;
        unsigned char   Flash_CRC;
    };      
    struct{
        BitWise_16      CAN_ID;
        unsigned char   Response_ID;
        unsigned char   EE_Address;
        unsigned char   EE_Data[4];
    };
    struct{
        BitWise_16      CAN_ID;
        unsigned char   Response_ID;
        unsigned int    Raw_A2D[3];
    };    
} CAN_Packet_Structure;



